package attributes;

// Defining the attributes
public class Attributes {
    int vitality;
    int strength;
    int dexterity;
    int intelligence;

    //Constructor
    public Attributes(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //Function to update attributes
    public void updateAttributes(Attributes attributes) {
        this.vitality += attributes.vitality;
        this.strength += attributes.strength;
        this.dexterity += attributes.dexterity;
        this.intelligence += attributes.intelligence;
    }


    //Returning the characters attributes as a String
    public String getAttributes() {
        return "vitality=" + vitality +
                " strength=" + strength +
                " dexterity=" + dexterity +
                " intelligence=" + intelligence;
    }

}
